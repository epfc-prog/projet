package eu.epfc.prog2;

import eu.epfc.prog2.controllers.DetailsController;
import eu.epfc.prog2.controllers.RootController;
import eu.epfc.prog2.models.Person;
import javafx.application.Application;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.BorderPane;
import javafx.stage.Stage;

import java.io.IOException;

public class HelloApplication extends Application {

    private static DetailsController detailsController;

    @Override
    public void start(Stage stage) throws IOException {
        FXMLLoader fxmlLoader = new FXMLLoader(HelloApplication.class.getResource("root-view.fxml"));

        BorderPane rootLayout = fxmlLoader.load();

        Scene scene = new Scene(rootLayout, 600, 400);

        stage.setTitle("Hello!");
        stage.setScene(scene);

        FXMLLoader detailsViewLoader = new FXMLLoader(HelloApplication.class.getResource("details-view.fxml"));
        AnchorPane detailLayout = detailsViewLoader.load();

        rootLayout.setCenter(detailLayout);

        ObservableList<Person> listePersonnes = FXCollections.observableArrayList();

        listePersonnes.add(new Person("Hans", "Muster"));
        listePersonnes.add(new Person("Ruth", "Mueller"));
        listePersonnes.add(new Person("Heinz", "Kurz"));
        listePersonnes.add(new Person("Cornelia", "Meier"));
        listePersonnes.add(new Person("Werner", "Meyer"));
        listePersonnes.add(new Person("Lydia", "Kunz"));
        listePersonnes.add(new Person("Anna", "Best"));
        listePersonnes.add(new Person("Stefan", "Meier"));
        listePersonnes.add(new Person("Martin", "Mueller"));

        detailsController = detailsViewLoader.getController();
        detailsController.setListePersonnes(listePersonnes);
        detailsController
                .getListePersonnesTableView()
                .getSelectionModel()
                .selectedItemProperty()
                .addListener(HelloApplication::unAutreElementSelecte);

        RootController rootController = fxmlLoader.getController();

        rootController.setListePersonnes(listePersonnes);

        // Permet d'afficher l'application
        stage.show();
    }

    public static void main(String[] args) {
        launch();
    }

    private static void unAutreElementSelecte(ObservableValue<? extends Person> obs, Person oldSelection, Person newSelection) {
        detailsController.setPersonneSelectionne(newSelection);
    }
}