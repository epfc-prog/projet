package eu.epfc.prog2.controllers;

import eu.epfc.prog2.models.Person;
import javafx.fxml.FXML;
import javafx.scene.control.TextField;
import javafx.stage.Stage;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

public class ModifierContactController {
    @FXML
    TextField prenomField;

    @FXML
    TextField nomField;

    @FXML
    TextField rueField;

    @FXML
    TextField villeField;

    @FXML
    TextField codePostalField;

    @FXML
    TextField dateNaissField;

    private Stage fenetre;
    private Person contactSelectione;

    public void setFenetreParent(Stage fenetre) {
        this.fenetre = fenetre;
    }

    public void setPerson(Person selectedItem) {
        this.contactSelectione = selectedItem;

        prenomField.setText(selectedItem.getFirstName());
        nomField.setText(selectedItem.getLastName());
        rueField.setText(selectedItem.getStreet());
        villeField.setText(selectedItem.getCity());
        codePostalField.setText(String.valueOf(selectedItem.getPostalCode()));
        dateNaissField.setText(selectedItem.getBirthday().format(DateTimeFormatter.ISO_DATE));
    }

    public void annulerHandler()
    {
        this.fenetre.close();
    }

    public void sauvegarderHandler()
    {
        DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");

        contactSelectione.setFirstName(prenomField.getText());
        contactSelectione.setLastName(nomField.getText());
        contactSelectione.setStreet(rueField.getText());
        contactSelectione.setCity(villeField.getText());
        contactSelectione.setPostalCode(Integer.parseInt(codePostalField.getText()));
        contactSelectione.setBirthday(dateTimeFormatter.parse(dateNaissField.getText(), LocalDate::from));

        this.fenetre.close();
    }
}
