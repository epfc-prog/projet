package eu.epfc.prog2.controllers;

import eu.epfc.prog2.HelloApplication;
import eu.epfc.prog2.models.Person;
import javafx.beans.value.ObservableValue;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Modality;
import javafx.stage.Stage;

import java.io.IOException;
import java.time.format.DateTimeFormatter;

public class DetailsController {
    @FXML
    private TableView<Person> listePersonnes;

    @FXML
    private TableColumn<Person, String> firstnameCell;

    @FXML
    private TableColumn<Person, String> lastnameCell;

    @FXML
    private Label prenomLabel;

    @FXML
    private Label nomLabel;

    @FXML
    private Label rueLabel;

    @FXML
    private Label villeLabel;

    @FXML
    private Label codePostalLabel;

    @FXML
    private Label dateNissLabel;

    @FXML
    private Button modifierBtn;

    @FXML
    private Button supprimerBtn;

    private static ObservableValue<String> assignerPrenom(TableColumn.CellDataFeatures<Person, String> personView) {
        return personView.getValue().firstNameProperty();
    }

    private static ObservableValue<String> assignerNom(TableColumn.CellDataFeatures<Person, String> personView) {
        return personView.getValue().lastNameProperty();
    }

    public void setListePersonnes(ObservableList<Person> listePersonnes) {
        firstnameCell.setCellValueFactory(DetailsController::assignerPrenom);
        lastnameCell.setCellValueFactory(DetailsController::assignerNom);

        this.listePersonnes.setItems(listePersonnes);
    }

    public void setPrenomLabel(String prenom) {
        this.prenomLabel.setText(prenom);
    }

    public TableView<Person> getListePersonnesTableView() {
        return listePersonnes;
    }

    public void setPersonneSelectionne(Person person) {
        if (person == null) {
            // Aucun élément est sélectionné.
            prenomLabel.setText("");
            nomLabel.setText("");
            rueLabel.setText("");
            villeLabel.setText("");
            codePostalLabel.setText("");
            dateNissLabel.setText("");

            modifierBtn.setDisable(true);
            supprimerBtn.setDisable(true);
        } else {
            // Un contact est sélectionné.
            prenomLabel.setText(person.getFirstName());
            nomLabel.setText(person.getLastName());
            rueLabel.setText(person.getStreet());
            villeLabel.setText(person.getCity());
            codePostalLabel.setText((String.valueOf(person.getPostalCode())));
            dateNissLabel.setText(person.getBirthday().format(DateTimeFormatter.ISO_DATE));

            modifierBtn.setDisable(false);
            supprimerBtn.setDisable(false);
        }
    }

    @FXML
    private void supprimerAction()
    {
        Person contact = listePersonnes.getSelectionModel().getSelectedItem();

        listePersonnes.getItems().remove(contact);
    }

    @FXML
    private boolean modifierAction()
    {
        FXMLLoader loader = new FXMLLoader();
        AnchorPane modifierVue;
        Stage modifierStage = new Stage();

        loader.setLocation(HelloApplication.class.getResource("modifier-contact.fxml"));

        try {
            modifierVue = loader.load();
        } catch (IOException e) {
            // TODO

            // Quitter la fonction
            return false;
        }

        modifierStage.setTitle("Modifier un contact");
        modifierStage.initModality(Modality.WINDOW_MODAL);

        Scene modifierScene = new Scene(modifierVue);

        modifierStage.setScene(modifierScene);

        // Récupérer le controlleur lié à la vue modifier-contact
        ModifierContactController controller = loader.getController();

        controller.setFenetreParent(modifierStage);

        // Récupérer le contact séléctionné.
        Person contactSelectionnee = listePersonnes.getSelectionModel().getSelectedItem();

        // Passer le contact séléctionné au controlleur lié la vue de modification
        controller.setPerson(contactSelectionnee);

        modifierStage.show();

        return true;
    }
}
