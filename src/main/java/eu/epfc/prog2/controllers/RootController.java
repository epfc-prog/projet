package eu.epfc.prog2.controllers;

import eu.epfc.prog2.models.Person;
import javafx.collections.ObservableList;

public class RootController {
    private ObservableList<Person> listePersonnes;

    public void setListePersonnes(ObservableList<Person> listePersonnes) {
        this.listePersonnes = listePersonnes;
    }

    public void enregistrerHandle()
    {
        System.out.println("<persons>");

        for(int i = 0; i < listePersonnes.size(); i++) {
            System.out.println("   <person>");

            Person personne = listePersonnes.get(i);

            System.out.println("        <nom>"+personne.getLastName()+"</nom>");
            System.out.println("        <prenom>"+personne.getLastName()+"</prenom>");
            System.out.println("        <dateNaissance>"+personne.getLastName()+"</dateNaissance>");
            System.out.println("        <rue>"+personne.getLastName()+"</rue>");
            System.out.println("        <ville>"+personne.getLastName()+"</ville>");
            System.out.println("        <codePostal>"+personne.getLastName()+"</codePostal>");

            System.out.println("   </person>");
        }

        System.out.println("</persons>");
    }
}
