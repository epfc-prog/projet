module eu.epfc.prog2 {
    requires javafx.controls;
    requires javafx.fxml;

    opens eu.epfc.prog2 to javafx.fxml;
    exports eu.epfc.prog2.controllers;
    opens eu.epfc.prog2.controllers to javafx.fxml;
    exports eu.epfc.prog2;
}